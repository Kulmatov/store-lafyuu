<p align="center"><a href=><img src=https://miro.medium.com/max/1200/1*slHeZngyeUr7ypEz7MNL5w.png width="400"></a></p>

[//]: # (<p align="center">)

[//]: # (<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>)

[//]: # (<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>)

[//]: # (<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>)

[//]: # (<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>)

[//]: # (</p>)

## About Django

Django is a high-level Python web framework that enables rapid development of secure and maintainable websites. Built by experienced developers, Django takes care of much of the hassle of web development, so you can focus on writing your app without needing to reinvent the wheel. It is free and open source, has a thriving and active community, great documentation, and many options for free and paid-for support.

- [Ridiculously fast](https://www.djangoproject.com/start/).
Django was designed to help developers take applications from concept to completion as quickly as possible.
- [Reassuringly secure.](https://docs.djangoproject.com/en/4.0/faq/general/#does-django-scale)
Django takes security seriously and helps developers avoid many common security mistakes.
-  [Exceedingly scalable. 
Some of the busiest sites on the web leverage Django’s ability to quickly and flexibly scale
- [Fully loaded.](https://docs.djangoproject.com/en/4.0/intro/overview/) Django includes dozens of extras you can use to handle common web development tasks. Django takes care of user authentication, content administration, site maps, RSS feeds, and many more tasks — right out of the box. 



## Learning Django

Django makes it easier to build better web apps more quickly and with less code. [documentation](https://docs.djangoproject.com/en/4.0/).


## Django Sponsors

The following organizations are corporate members of the Django Software Foundation. If you are interested in becoming a corporate member of the DSF, you can find out more on our [corporate membership page](https://www.djangoproject.com/foundation/corporate-membership/)
### Premium Partners

- **[DEFNA](https://www.defna.org/)**
- **[pretix](https://pretix.eu/)**
- **[American Express](https://www.americanexpress.com/)**
- **[CryptAPI](http://www.cryptapi.io/)**
- **[Sentry](https://sentry.io/for/django/)**
- **[Brilliant](https://brilliant.org/)**
- **[BS Custom Software Solutions](https://www.jbssolutions.com/)**


## Contributing

Django is a community that lives on its volunteers. As it keeps growing, we always need more people to help others. You can contribute in many ways, either on the framework itself or in the wider ecosystem.


## Design my project

[Lafyuu](https://www.figma.com/file/vkk2i7YvZpWw0LS8lwa26Z/Lafyuu-E-commerce-UI-Kit-for-Figma-(Community)?node-id=0%3A1)